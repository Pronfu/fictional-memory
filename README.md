# Fictional Memory

Want to forget a memory or thought? Enter it here and watch it disappear, nothing is save here on site and you no longer need to remember it.

## Installation

Clone or download the repo, copy it to your folder of choice and either open it in your web browser or copy it to your web server.

## Live Demo
[https://projects.gregoryhammond.ca/fictional-memory/](https://projects.gregoryhammond.ca/fictional-memory/)

## License
[Unlicense](https://unlicense.org/)