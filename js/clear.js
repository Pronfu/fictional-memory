/* Much thanks to https://stackoverflow.com/a/2783639 for the basis of this */

function clearContents(forgetThis) 
{
  forgetThis.value = '';
}